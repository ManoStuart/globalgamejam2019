public enum RoomType
{
    Bedroom = 1 << 0,
    Bathroom = 1 << 1,
    Kitchen = 1 << 2,
    LivingRoom = 1 << 3
}

public enum Sides
{
    Left,
    Right,
    Top,
    Bottom
}

public enum FurnitureType
{
    Bed,
    KitchenSink,
    BathroomSink,
    Toilet,
    Shower,
    Table,
    Chair,
    Television,
    Computer,
    Vase,
    Box,
    Wardrobe,
    Shelves,
    Fridge,
    Stove,
    Counter,
    Door
}