using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class FurnitureRestrictions
{
    private const int furnitureResourcesCount = 34;
    private static readonly Dictionary<FurnitureType, List<FurnitureRestriction>> furnitureRestrictions = new Dictionary<FurnitureType, List<FurnitureRestriction>>()
    {
        {FurnitureType.Bed, new List<FurnitureRestriction>()},
        {FurnitureType.KitchenSink, new List<FurnitureRestriction>()},
        {FurnitureType.BathroomSink, new List<FurnitureRestriction>()},
        {FurnitureType.Toilet, new List<FurnitureRestriction>()},
        {FurnitureType.Shower, new List<FurnitureRestriction>()},
        {FurnitureType.Table, new List<FurnitureRestriction>()},
        {FurnitureType.Chair, new List<FurnitureRestriction>()},
        {FurnitureType.Television, new List<FurnitureRestriction>()},
        {FurnitureType.Computer, new List<FurnitureRestriction>()},
        {FurnitureType.Vase, new List<FurnitureRestriction>()},
        {FurnitureType.Box, new List<FurnitureRestriction>()},
        {FurnitureType.Wardrobe, new List<FurnitureRestriction>()},
        {FurnitureType.Shelves, new List<FurnitureRestriction>()},
        {FurnitureType.Fridge, new List<FurnitureRestriction>()},
        {FurnitureType.Stove, new List<FurnitureRestriction>()},
        {FurnitureType.Counter, new List<FurnitureRestriction>()},
    };

    public static List<FurnitureType> furnitureTypes = new List<FurnitureType>()
    {
        FurnitureType.Bed,
        FurnitureType.KitchenSink,
        FurnitureType.BathroomSink,
        FurnitureType.Toilet,
        FurnitureType.Shower,
        FurnitureType.Table,
        FurnitureType.Chair,
        FurnitureType.Television,
        FurnitureType.Computer,
        FurnitureType.Vase,
        FurnitureType.Box,
        FurnitureType.Wardrobe,
        FurnitureType.Shelves,
        FurnitureType.Fridge,
        FurnitureType.Stove,
        FurnitureType.Counter,
    };

    public static void SetUp()
    {
        for (int i = 0; i < furnitureResourcesCount; i++)
        {
            var furniture = Resources.Load<FurnitureRestriction>($"FurnitureRestrictions/Furniture{i.ToString()}");
            var width = furniture.width;
            var tileArray = furniture.tileArray;
            furniture.tiles = new Tile[tileArray.Length / width, width];

            for (int j = 0; j < tileArray.Length; j++)
                furniture.tiles[j / width, j % width] = tileArray[j];
            
            furnitureRestrictions[furniture.furnitureType].Add(furniture); 
        }
    }

    public static FurnitureRestriction GetRandomFurnitureRestrictionsOfType(FurnitureType furnitureType)
    {
        var list = furnitureRestrictions[furnitureType];
        Utils.Shuffle(list);
        
        return list[0];
    }
    
    public static FurnitureRestriction GetRandomFurnitureRestrictionsForRoomType(RoomType roomType)
    {
        var roomRestriction = RoomRestrictions.GetRoomRestrictions(roomType);
        var allowedFurniture = roomRestriction.allowedFurniture.ToList();
        
        Utils.Shuffle(allowedFurniture);
        
        var list = furnitureRestrictions[allowedFurniture[0]];
        Utils.Shuffle(list);
        
        return list[0];
    }
    
    public static FurnitureRestriction GetFurnitureRestrictions(FurnitureType furnitureType, string name)
    {
        var list = furnitureRestrictions[furnitureType];

        foreach (var furniture in list)
        {
            if (furniture.name == name)
                return furniture;
        }

        throw new Exception();
    }
}