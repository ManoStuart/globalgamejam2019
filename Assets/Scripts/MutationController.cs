using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MutationController
{
    private const int mutatationCount = 3;
    
    private delegate bool MutationFunction(Home home);

    private static List<MutationFunction> mutationFunctions = new List<MutationFunction>()
    {
        AddRoom,
        RemoveRoom,
//        Relink
    };
    
    public static Home Mutate(Home home)
    {
        var newHome = new Home();
        newHome.SetUp(home);
        
        for (int i = 0; i < mutatationCount; i++)
        {
            Utils.Shuffle(mutationFunctions);

            foreach (var crossFunction in mutationFunctions)
            {
                if (crossFunction(newHome))
                    break;
            }
        }
        
        newHome.CenterHome();

        return newHome;
    }

    private static bool AddRoom(Home home)
    {
        if (home.rooms.Count >= Home.maxRoomCount)
            return false;
        
        Utils.Shuffle(Room.roomTypes);
        
        var newRoom = new Room(Room.roomTypes[0]);
        home.AddRoom(newRoom);
        home.PositionRoom(newRoom);
        
        return true;
    }
    
    private static bool RemoveRoom(Home home)
    {
        Utils.Shuffle(home.rooms);

        foreach (var room in home.rooms)
        {
            if (home.GetRoomTypeCount(room.roomType) > Home.mustHaveRooms[room.roomType])
            {
                home.RemoveRoom(room);
                return true;
            }
        }

        return false;
    }
    
    private static bool Relink(Home home)
    {
        return true;
    }
}