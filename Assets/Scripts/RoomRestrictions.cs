using System.Collections.Generic;
using UnityEngine;

public static class RoomRestrictions
{
    private static readonly Dictionary<RoomType, RoomRestriction> restrictions = new Dictionary<RoomType, RoomRestriction>()
    {
        {RoomType.Bedroom, Resources.Load<RoomRestriction>("RoomRestrictions/Bedroom")},
        {RoomType.Bathroom, Resources.Load<RoomRestriction>("RoomRestrictions/Bathroom")},
        {RoomType.LivingRoom, Resources.Load<RoomRestriction>("RoomRestrictions/LivingRoom")},
        {RoomType.Kitchen, Resources.Load<RoomRestriction>("RoomRestrictions/Kitchen")},
    };

    public static RoomRestriction GetRoomRestrictions(RoomType roomType)
    {
        return restrictions[roomType];
    }
}