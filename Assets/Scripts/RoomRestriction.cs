using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoomRestriction : ScriptableObject {
    public int[] minSize;
    public int[] maxSize;
    public Tile floor;
    public Tile[] wall = new Tile[2];
    public Tile[] door = new Tile[2];

    public FurnitureType[] mustHaveFurniture;
    public FurnitureType[] allowedFurniture;
}
