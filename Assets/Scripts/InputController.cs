using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    public enum InputMode {Selection, Inspection, Confirmation, TutorialLoop, TutorialZoom}

    public static InputMode inputMode = InputMode.TutorialLoop;
    
    private Vector3 dragOrigin; //Where are we moving?
    private Vector3 clickOrigin = Vector3.zero; //Where are we starting?
    private Vector3 basePos = Vector3.zero; //Where should the camera be initially?

    public Image tutorialLoop;
    public Image tutorialZoom;

    void Start()
    {
        tutorialLoop.enabled = true;
        tutorialZoom.enabled = true;
    }
    
    void Update() {
        switch(inputMode)
        {
            case InputMode.TutorialLoop:
                TryNextTutorial();
                break;
            case InputMode.TutorialZoom:
                TryEndTutorial();
                break;
            case InputMode.Selection:
                TrySelectHome();
                TryInspectHome();
                break;
            case InputMode.Inspection:
                CameraDrag();
                TryBackToSelection();
                break;
            case InputMode.Confirmation:
                TryConfirm();
                TryCancelSelection();
                break;
        }
    }

    private HomeView GetTargetHome()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);

        if (!hit)
            return null;
        
        var selected = hit.collider.gameObject;
        return selected.GetComponent<HomeView>();
    }
    
    private void TrySelectHome()
    {
        if (!Input.GetMouseButtonDown(0))
            return;
        
        var homeView = GetTargetHome();

        if (!homeView)
            return;
        
        EvolutionManager.instance.ToggleHomeSelection(homeView);
    }

    private void TryInspectHome()
    {
        if (!Input.GetMouseButtonDown(1))
            return;
        
        var homeView = GetTargetHome();

        if (!homeView)
            return;

        EvolutionManager.instance.FocusHome(homeView.gameObject);
        inputMode = InputMode.Inspection;
    }

    private void TryBackToSelection()
    {
        if (!Input.GetMouseButtonDown(1))
            return;
        
        Camera.main.transform.position = new Vector3(0, 0, -10);
        EvolutionManager.instance.ResetFocus();
        inputMode = InputMode.Selection;
    }

    private void CameraDrag()
    {
        if (Input.GetMouseButton(0))
        {
            if (clickOrigin == Vector3.zero) {
                clickOrigin = Input.mousePosition;
                basePos = Camera.main.transform.position;
            }
            dragOrigin = Input.mousePosition;
        }
        else
        {
            clickOrigin = Vector3.zero;
            return;
        }
 
        Camera.main.transform.position = new Vector3(
            basePos.x + ((clickOrigin.x - dragOrigin.x) * .01f),
            basePos.y + ((clickOrigin.y - dragOrigin.y) * .01f),
            -10
        );
    }

    private void TryConfirm()
    {
        if (!Input.GetMouseButtonDown(0))
            return;
        
        EvolutionManager.instance.GenerateChildren();
        inputMode = InputMode.Selection;
    }

    private void TryCancelSelection()
    {
        if (!Input.GetMouseButtonDown(1))
            return;
        
        EvolutionManager.instance.ResetSelection();
        inputMode = InputMode.Selection;
    }

    private void TryNextTutorial()
    {
        if (!Input.GetMouseButtonDown(1) && !Input.GetMouseButtonDown(0))
            return;

        inputMode = InputMode.TutorialZoom;
        tutorialLoop.enabled = false;
    }
    private void TryEndTutorial()
    {
        if (!Input.GetMouseButtonDown(1) && !Input.GetMouseButtonDown(0))
            return;
        
        inputMode = InputMode.Selection;
        tutorialZoom.enabled = false;
    }
}