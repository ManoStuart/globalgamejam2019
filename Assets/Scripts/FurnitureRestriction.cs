using UnityEngine;
using UnityEngine.Tilemaps;

public class FurnitureRestriction : ScriptableObject
{
    public Tile[] tileArray;
    public int width;
    public Tile[,] tiles;
    public FurnitureType furnitureType;
    public string name;
}
