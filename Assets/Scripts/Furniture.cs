using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.Tilemaps;

public class Furniture
{
    private static int instanceCount = 0;
    
    public FurnitureType furnitureType;
    public string name;
    public int id;
    public int[] position;
    public int[] size;
    public FurnitureRestriction furnitureRestriction;

    #region Constructors

    public Furniture(RoomType roomType)
    {
        furnitureRestriction = FurnitureRestrictions.GetRandomFurnitureRestrictionsForRoomType(roomType);

        furnitureType = furnitureRestriction.furnitureType;
        name = furnitureRestriction.name;
        
        SetId();
        SetSize(furnitureRestriction);
    }
    
    public Furniture(FurnitureType furnitureType)
    {
        furnitureRestriction = FurnitureRestrictions.GetRandomFurnitureRestrictionsOfType(furnitureType);

        this.furnitureType = furnitureRestriction.furnitureType;
        name = furnitureRestriction.name;
        
        SetId();
        SetSize(furnitureRestriction);
    }

    public Furniture(FurnitureRestriction restriction)
    {
        furnitureRestriction = restriction;

        furnitureType = furnitureRestriction.furnitureType;
        name = furnitureRestriction.name;
        
        SetId();
        SetSize(furnitureRestriction);
    }

    public Furniture(Furniture furnitureModel)
    {
        furnitureType = furnitureModel.furnitureType;
        name = furnitureModel.name;
        position = new[] {furnitureModel.position[0], furnitureModel.position[1]};
        furnitureRestriction = furnitureModel.furnitureRestriction;
        
        SetId();
        SetSize(GetRestrictions());
    }
    
    #endregion

    public FurnitureRestriction GetRestrictions()
    {
        return furnitureRestriction;
    }
    
    private void SetId()
    {
        id = instanceCount;
        instanceCount++;
    }

    private void SetSize(FurnitureRestriction restrictions)
    {
        size = new[]
        {
            restrictions.width,
            restrictions.tileArray.Length / restrictions.width
        };
    }

    public bool Intersects(Furniture otherFurniture)
    {
        Rect furnitureRect = new Rect(position[0], position[1], size[0], size[1]);
        Rect otherFurnitureRect = new Rect(otherFurniture.position[0], otherFurniture.position[1], otherFurniture.size[0], otherFurniture.size[1]);

        return furnitureRect.Overlaps(otherFurnitureRect);
    }
    
    public void SetPosition(int[] newPosition)
    {
        position = newPosition;
    }
    
    public int[] GetEdges()
    {
        return new[]
        {
            position[0],
            position[0] + size[0] - 1,
            position[1],
            position[1] + size[1] - 1
        };
    }

    public int[] RealWorldEdges(Room parentRoom)
    {
        var edges = GetEdges();
        edges[0] += parentRoom.position[0];
        edges[1] += parentRoom.position[0];
        edges[2] += parentRoom.position[1];
        edges[3] += parentRoom.position[1];

        return edges;
    }
}