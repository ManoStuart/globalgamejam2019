﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;

public class HomeView : MonoBehaviour
{
    public Tilemap roomsTilemap;
    public Tilemap selectionTilemap;
    public Tilemap wallTilemap;
    public Tilemap furnitureTilemap;
    public Tile selectionTile;
    public Tile ceilingTile;
    public Home home;
    public Color[] selectionColors;
    public bool selected;

    public void HomeSetUp(Home newHome)
    {
        home = newHome;        
        BuildHome();

        var collider = GetComponent<BoxCollider2D>();
        collider.size = new Vector2(home.width + 1, home.heigth + 1);
    }

    private void BuildHome()
    {
        foreach (var room in home.rooms)
        {
            var roomRestriction = RoomRestrictions.GetRoomRestrictions(room.roomType);
            var edges = room.GetEdges();

            FillFloor(edges, roomRestriction.floor);
            FillBorders(edges, ceilingTile, wallTilemap);
            FillTopWalls(edges, roomRestriction.wall);
            FillFurniture(room);
        }

        FillBorders(new []{home.minX - 2, home.maxX + 2, home.minY - 2, home.maxY + 2}, selectionTile, selectionTilemap);
    }

    private void FillFloor(int[] edges, Tile tile)
    {
        for (int i = edges[0]; i <= edges[1]; i++)
        {
            for (int j = edges[2]; j <= edges[3]; j++)
            {
                roomsTilemap.SetTile(new Vector3Int(i,j,0), tile);
            }
        }
    }
    
    private void FillBorders(int[] edges, Tile tile, Tilemap tilemap)
    {
        for (int i = edges[0]; i <= edges[1]; i++)
        {
            tilemap.SetTile(new Vector3Int(i, edges[2],0), tile);
            tilemap.SetTile(new Vector3Int(i, edges[3],0), tile);
        }
        for (int j = edges[2]; j <= edges[3]; j++)
        {
            tilemap.SetTile(new Vector3Int(edges[0], j,0), tile);
            tilemap.SetTile(new Vector3Int(edges[1], j,0), tile);
        }
    }

    private void FillTopWalls(int[] edges, Tile[] tiles)
    {
        for (int i = edges[0] + 1; i <= edges[1] - 1; i++)
        {
            wallTilemap.SetTile(new Vector3Int(i, edges[3] - 1,0), tiles[0]);
            wallTilemap.SetTile(new Vector3Int(i, edges[3] - 2,0), tiles[1]);
        }
        
    }

    private void FillFurniture(Room room)
    {
        foreach (var furniture in room.furnitures)
        {
            var tiles = furniture.GetRestrictions().tiles;
            var edges = furniture.RealWorldEdges(room);

            for (int i = edges[0]; i <= edges[1]; i++)
            {
                for (int j = edges[2]; j <= edges[3]; j++)
                {
                    var tile = tiles[edges[3] - j, i - edges[0]];
                    furnitureTilemap.SetTile(new Vector3Int(i, j,0), tile);
                }
            }
        }
    }
    
    public void SetSelected(int place)
    {
        selected = true;
        selectionTilemap.color = selectionColors[place];
    }

    public void RemoveSelection()
    {
        selected = false;
        selectionTilemap.color = Color.black;
    }

    public bool IsSelected()
    {
        return selected;
    }
}
